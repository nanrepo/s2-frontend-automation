import { testController } from '../support/world'
import { select } from '../support/utils'
import { ClientFunction } from 'testcafe';

const dotenv = require('dotenv');

export class Login {
  constructor () {
    dotenv.config();
    this.url = process.env.URL_HOST + `login`
    this.urlHome = process.env.URL_HOST + `apps/home`
  }
//LOGIN SELECTORS
  emailImput  () {
    return select('div.MuiFormControl-root:nth-child(1) > div:nth-child(2) > input:nth-child(1)')
  }

  passwordImput () {
    return select('div.MuiFormControl-root:nth-child(2) > div:nth-child(2) > input:nth-child(1)')
  }

  webtoolDescription () {
    return select('h6.MuiTypography-root:nth-child(3)').exists
  }
  
  searchMasImg () {
    return select ('.w-128').exists
  }
  
  webtoolTitle () {
    return select ('h3.MuiTypography-root').innerText
  }

  loginButton () {
    return select('.MuiButton-label');
  }


  async navigate () {
    await testController.navigateTo(this.url)
  }

  async navigateHome () {
    dotenv.config();
    this.urlHome = process.env.URL_HOST + `apps/home`;
    console.log(this.urlHome);
    const getLocation = ClientFunction(() => document.location.href).with({ boundTestRun: testController });
    await testController.expect(getLocation()).contains(this.urlHome);
  }
  async navigateHomeAndExit () {
    dotenv.config();
    this.urlHome = process.env.URL_HOST + `apps/home`;
    console.log(this.urlHome);
    const getLocation = ClientFunction(() => document.location.href).with({ boundTestRun: testController });
    await testController
    .expect(getLocation()).contains(this.urlHome)
    .click(this.logoutButton())
    .expect(getLocation()).contains(this.url);
  }

  async login (email) {
    await testController
      .click(this.emailImput())
      .typeText(this.emailImput(), email, { paste: true })
      .click(this.passwordImput())
      .typeText(this.passwordImput(), process.env.PASSWORD, { paste: true })
      .click(this.loginButton())
  }

  async loginValidate () {
    const webtoolTitle = this.webtoolTitle();
    const webtoolDescription = this.webtoolDescription();
    const searchMasImg = this.searchMasImg();

    await testController
      .expect(webtoolDescription).ok('El titulo de Semillas se visualiza', { allowUnawaitedPromise: false })
      .expect(searchMasImg).ok('El logo de searchmas se visualiza', { allowUnawaitedPromise: false })
      .expect(webtoolTitle).eql("Bienvenido a SearchMAS")
  }
}
