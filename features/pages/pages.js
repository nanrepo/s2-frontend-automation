import { Login } from './login.page';
import { Home } from './home.page';

export const pages = {
  login: new Login(),
  home: new Home(),

};
